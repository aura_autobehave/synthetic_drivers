import random
from math import pi

import bpy
import numpy as np


# Maths utils
def rd(a, d):
    if type(a) in [float, int]:
        if d:
            return int(180 * a / pi)
        else:
            return pi * a / 180.
    else:
        try:
            iterator = iter(a)
        except TypeError as te:
            raise ValueError('Cant convert to radians ', a)
        return type(a)([r(k) for k in a])


def r(a):
    return rd(a, d=False)


def d(a):
    return rd(a, d=True)


def dist(a, b):
    return sum([k ** 2 for k in (a - b)]) ** 0.5


# Blender utils
C = bpy.context
D = bpy.data


def hide_object(obj, hide=True):
    for o in obj.children:
        hide_object(o, hide=hide)
    obj.hide_set(hide)
    obj.hide_select = hide
    if 'Plane' in obj.name or 'OUT_' in obj.name or 'IN_' in obj.name:
        obj.hide_render = True
        obj.hide_set(True)
    else:
        obj.hide_render = hide


def get_object(name):
    return bpy.data.objects[name]


def unselect_all():
    select_only(None)


def select_only(obj=None):
    for ob in bpy.data.objects:
        ob.select_set(False)

    if obj is None:
        return

    obj.select_set(True)
    bpy.context.view_layer.objects.active = obj


def select_only_bone(armature, bone=None):
    for bo in armature.bones:
        bo.select = False

    if bone is None:
        return

    bone.select = True


def select_only_edit_bone(armature, bone=None):
    for bo in armature.edit_bones:
        bo.select = False
        bo.select_head = False
        bo.select_tail = False

    if bone is None:
        return

    bone.select = True
    bone.select_head = True
    bone.select_tail = True
    armature.edit_bones.active = bone


def _get_bone_pose(bone, struct, side):
    if isinstance(struct, str):
        struct = get_object(struct)

    current_mode = bpy.context.mode
    bpy.ops.object.mode_set(mode='OBJECT')
    matrix_world = struct.matrix_world
    bpy.ops.object.mode_set(mode=current_mode)
    if side == "tail":
        side_3d = struct.pose.bones[bone].tail
    else:
        side_3d = struct.pose.bones[bone].head

    return (matrix_world @ side_3d.to_4d()).to_3d()


def get_head_pose(bone, struct='Subject'):
    return _get_bone_pose(bone, struct, side='head')


def get_tail_pose(bone, struct='Subject'):
    return _get_bone_pose(bone, struct, side='tail')


# Other utils
def mat_to_list(mat):
    return np.array(mat).tolist()


class Randomizer:
    def __init__(self, objects):
        self.objects = objects
        for obj in self:
            hide_object(obj)

    def to_list(self):
        return [obj if isinstance(obj, bpy.types.Object) else obj.model for obj in self.objects]

    def __iter__(self):
        return iter(self.to_list())

    def __getitem__(self, item):
        return self.to_list()[item]

    def __len__(self):
        return len(self.objects)

    def get(self, pick_idx):
        pick = self.objects[pick_idx]
        # pick = pick if isinstance(pick, bpy.types.Object) else pick(*args, **kwargs)
        self.swap_object(self[pick_idx])
        return pick

    def __call__(self):
        pick_idx = random.randint(0, len(self) - 1)
        return self.get(pick_idx)

    def swap_object(self, obj=None):
        for o in self:
            if not o.hide_get():
                hide_object(o)

        if obj is not None:
            hide_object(obj, hide=False)


class StopError(Exception):
    pass
