from math import cos, sin, pi, acos

from mathutils import Vector

from scripts import utils

import importlib

importlib.reload(utils)
from scripts.utils import *


def default_rots():
    return {
        'hand_r': (0, 0, 0),
        'hand_l': (0, 0, 0),
        'thigh_r': (r(-70), r(-5 - random.random() * 15), 0),
        'thigh_l': (r(-70), r(5 + random.random() * 15), 0),
        'calf_r': (r(20 + random.random() * 50), r(30), 0),
        'calf_l': (r(20 + random.random() * 50), r(-30), 0)
    }


def bounds():
    values = {
        'spine_03': [[0, 40], 45, None],
        'upperarm_l': [None, 10, 10],
        'upperarm_r': [None, 25, 20],
        'neck_01': [[0, 40], 0, 10],
        'lowerarm_X': [[-25, 50], [0, 35], None]
    }

    temp = values.copy()
    for k, v in temp.items():
        if '_X' in k:
            values.update({k.replace('X', u): v for u in ['l', 'r']})
            del (values[k])

    for k, v in values.items():
        values[k] = [[-a, a] if isinstance(a, int) else a for a in v]

    return values


def get_angles(angs):
    new_angs = []
    for ang in angs:
        if isinstance(ang, list):
            new_angs.append(random.randint(*ang))
        elif isinstance(ang, int):
            new_angs.append(random.randint(-ang, ang))
        elif ang is None:
            new_angs.append(0)

    return Vector(r(new_angs))


def reset_subject(subject):
    bpy.ops.object.mode_set(mode='POSE')
    subject.location = [0, 0, 0]
    subject.rotation_euler = [0, 0, 0]

    for bone in subject.pose.bones:
        bone.rotation_mode = 'XYZ'
        if bone.name in ["Root", "pelvis"]:
            continue
        if '_IK' in bone.name:
            bone.location = (0, 1.2, 0.25)
        else:
            bone.location = (0, 0, 0)
        bone.rotation_euler = (0, 0, 0)

    bpy.ops.object.mode_set(mode='OBJECT')
    subject.scale = [0.9] * 3
    sit_height = utils.get_head_pose('pelvis', subject).z * 0.88
    subject.location = [0, -0.04, - sit_height]
    subject.rotation_euler = r([-16, 0, 0])


def hand_pose(pose, side, grasp=None):
    if grasp is None:
        hand_ratio = random.uniform(0.1, 0.8)
    elif grasp is True:
        hand_ratio = random.uniform(0.5, 0.8)
    elif grasp is False:
        hand_ratio = random.uniform(0.05, 0.15)
    else:
        hand_ratio = 0

    for finger in ['thumb', 'index', 'middle', 'ring', 'pinky']:
        angles = r([40, 40, 40]) if finger == 'thumb' else r([70, 90, 40])
        solo_ratio = random.uniform(0.7, 1) * hand_ratio
        for i in range(3):
            pose.bones[f'{finger}_{i + 1:02}_{side}'].rotation_euler.x = angles[i] * solo_ratio


def random_pose_ik(subject, auto_ik=False, targets=None, id_targets=None):
    """
        1- reset and fix legs
        2- randomize back
        2b- bend back when too much twisted
        3- randomize neck (backward proportional to back bending)
        4- move arms with IK
        :param subject: subject Object
        :param auto_ik: use auto_ik option
        :param targets: choose among fixed wrist targets
        :return:
        """
    # 1
    bpy.ops.object.mode_set(mode='OBJECT')

    pose = subject.pose
    select_only(subject)

    def rota(bone):
        return Vector(pose.bones[bone].rotation_euler)

    reset_subject(subject)
    arm_length = dist(get_head_pose('upperarm_l', subject), get_head_pose('hand_l', subject))

    base_rots = default_rots()
    matrix_world = C.active_object.matrix_world.copy()
    bpy.ops.object.mode_set(mode='POSE')

    bounds_vals = bounds()
    for bone, angles in base_rots.items():
        pose.bones[bone].rotation_euler = angles

    if targets is None:
        # 2
        pose.bones['spine_03'].rotation_euler = get_angles(bounds_vals['spine_03'])

        # 2b Compensate for shoulder in seat by bending back to front
        pose.bones['spine_01'].rotation_euler = r(
            Vector((random.randint(0, 10) + max(d(abs(rota('spine_03').y)) - 20, 0) / 2, 0,
                    0))) + rota('spine_01')

        # 3
        pose.bones['neck_01'].rotation_euler = (
                get_angles(bounds_vals['neck_01']) +
                get_angles(
                    [[d((rota('spine_01').x + rota('spine_03').x) * -0.5), 0], None, None]) +
                Vector((0, random.uniform(0, 0.5) * rota('spine_03').y, 0))
        )

    else:
        pose.bones['spine_03'].rotation_euler = get_angles([[5, 20], 15, None])
        pose.bones['neck_01'].rotation_euler = get_angles([[5, 25], 0, 10])

    pose.use_auto_ik = auto_ik

    for s in ['l', 'r']:
        # Disconnect clavicle
        armature = bpy.data.armatures[subject.name]

        if auto_ik:
            pose_bone = pose.bones[f'hand_{s}']
        else:
            pose_bone = pose.bones[f'hand_{s}_IK']

        bone = pose_bone.bone
        select_only_bone(armature, bone)

        if targets is None:
            target = Vector()
            shoulder_pose = get_head_pose(f'upperarm_{s}', subject)

            back_forward_angle = rota('spine_03').x + rota('spine_01').x - r(30)  # 0 = straight

            phi = random.uniform(
                max((r(-160) if s == 'r' else r(-100)) + rota('spine_03').y, r(-160)),
                min((r(-80) if s == 'r' else r(-40)) + rota('spine_03').y, r(-20))
            )
            theta_bound = 0.8
            costheta = random.uniform(max(-0.8, -cos(theta_bound - back_forward_angle - rota('neck_01').x)),
                                      min(0.8, cos(theta_bound + back_forward_angle + rota('neck_01').x)))

            theta = acos(costheta)

            min_arm_factor = 0.2 + max(sin(back_forward_angle), 0)
            u = random.uniform(min_arm_factor, 1) * arm_length

            target.x = u * sin(theta) * cos(phi)
            target.y = u * sin(theta) * sin(phi)
            target.z = u * cos(theta)

            target += shoulder_pose

            hand_pose(pose, side=s)
        else:
            if id_targets is None:
                target = random.choice(targets[s])
            else:
                try:
                    target = targets[s][id_targets[s]]
                except IndexError as err:
                    print(targets[s], id_targets, s)
                    raise err
            pose.bones[f'hand_{s}_IK'].rotation_euler = Vector((0, 0, 0))
            pose.bones[f'hand_{s}_IK'].rotation_euler = (
                ((matrix_world @ pose.bones[f'hand_{s}_IK'].matrix).inverted() @ target.matrix_world).to_euler())
            hand_pose(pose, side=s, grasp="_close" in target.name)

            target = target.location

        location = get_head_pose(bone.name, subject)
        bpy.ops.transform.translate(value=target - location)

    if targets is not None:
        for s in 'lr':
            subject.pose.bones[f'lowerarm_{s}'].constraints['IK'].chain_count = 6

    bpy.ops.object.mode_set(mode='OBJECT')


if __name__ == '__main__':
    subject = get_object('Subject')
