import json
import glob
import os

from mathutils import Vector, Matrix
import colorsys

from scripts import utils

import importlib

importlib.reload(utils)
from scripts.utils import *

D = bpy.data

# Load clothes list
with open(r"mh_models\mh_mass_produce.json") as f:
    data = json.load(f)
    hairs = [name.replace(' ', '_') for name in data['hair']['allNames']]
    tops = []
    bottoms = []
    suits = []
    for name, cloth in data['clothes']['clothesInfos'].items():
        if cloth["maleUpper"] or cloth["femaleUpper"] or cloth["mixedUpper"]:
            tops.append(name.replace(' ', '_'))
        if cloth["maleLower"] or cloth["femaleLower"] or cloth["mixedLower"]:
            bottoms.append(name.replace(' ', '_'))
        if cloth["maleFull"] or cloth["femaleFull"] or cloth["mixedFull"]:
            bottoms.append(name.replace(' ', '_'))


class Human:
    def __init__(self, model):
        self.model = model
        self.name = self.model.name

        self.init_textures()
        self.init_bones()
        self.top = self.init_color_clothes(tops + suits)
        self.bot = self.init_color_clothes(bottoms + suits)

        self.hairs = self.init_color_hairs(hairs)

    def init_color_clothes(self, clothes):
        mat = None
        for obj in self.model.children:
            for clo in clothes:
                if clo.lower() in obj.name.lower():
                    if not len(obj.material_slots):
                        break
                    utils.select_only(obj)
                    mat = obj.material_slots[0].material
                    mat.use_nodes = True
                    obj.active_material = mat
                    nodes = mat.node_tree.nodes
                    node_ramp = nodes.new("ShaderNodeValToRGB")
                    node_ramp.color_ramp.elements.remove(node_ramp.color_ramp.elements[0])
                    node_ramp.color_ramp.elements[0].position = 1.
                    node_ramp.color_ramp.color_mode = 'HSV'

                    input_links = nodes['Principled BSDF'].inputs[0].links
                    nodes["Principled BSDF"].inputs["Roughness"].default_value = max(
                        nodes["Principled BSDF"].inputs["Roughness"].default_value, 0.35)

                    if len(input_links):
                        img_node = nodes['Principled BSDF'].inputs[0].links[0].from_node

                        node_mix = nodes.new("ShaderNodeMixRGB")
                        node_mix.blend_type = 'MIX'
                        node_mix.inputs[0].default_value = 0.5

                        mat.node_tree.links.new(img_node.outputs[0], node_mix.inputs[1])
                        mat.node_tree.links.new(node_ramp.outputs[0], node_mix.inputs[2])
                        mat.node_tree.links.new(node_mix.outputs[0], nodes['Principled BSDF'].inputs[0])
                    else:
                        mat.node_tree.links.new(node_ramp.outputs[0], nodes['Principled BSDF'].inputs[0])

                    node_ramp.color_ramp.elements[0].color = random_HSV()
                    node_ramp.color_ramp.elements[0].color = random_HSV()
                    break

        return mat

    def init_color_hairs(self, hairs):
        mat = None
        for obj in self.model.children:
            for hair in hairs:
                if hair.lower() in obj.name.lower():
                    if not len(obj.material_slots):
                        break

                    utils.select_only(obj)
                    mat = obj.material_slots[0].material
                    mat.use_nodes = True
                    obj.active_material = mat
                    nodes = mat.node_tree.nodes

                    input_links = nodes['Principled BSDF'].inputs[0].links
                    nodes["Principled BSDF"].inputs["Roughness"].default_value = max(
                        nodes["Principled BSDF"].inputs["Roughness"].default_value, 0.5)

                    if len(input_links):
                        img_node = nodes['Principled BSDF'].inputs[0].links[0].from_node

                        node_hsv = nodes.new("ShaderNodeHueSaturation")
                        node_hsv.inputs[1].default_value = 0.

                        node_mix = nodes.new("ShaderNodeMixRGB")
                        node_mix.blend_type = 'ADD'
                        node_mix.inputs[0].default_value = 1.
                        node_mix.inputs[2].default_value = random_color_hair()

                        mat.node_tree.links.new(img_node.outputs[0], node_hsv.inputs[4])
                        mat.node_tree.links.new(node_hsv.outputs[0], node_mix.inputs[1])
                        mat.node_tree.links.new(node_mix.outputs[0], nodes['Principled BSDF'].inputs[0])

                    break
        return mat

    def init_bones(self):
        armature = bpy.data.armatures[self.model.name]
        bpy.ops.object.mode_set(mode='EDIT')
        for s in ('l', 'r'):
            # Disconnect clavicle
            armature.edit_bones[f'clavicle_{s}'].use_connect = False

            # Create IK
            hand = armature.edit_bones[f'hand_{s}']
            hand_ik = armature.edit_bones.new(f'hand_{s}_IK')
            utils.select_only_edit_bone(armature, hand_ik)
            hand_ik.length = hand.length
            bpy.ops.transform.resize(value=Vector((1.5, 1.5, 1.5)))

        bpy.ops.object.mode_set(mode='POSE')
        ik_constraints = {
            'upperarm': [(-45, 120), (-90, 90), (-80, 80)],
            'lowerarm': [(-30, 120), None, None],
            'clavicle': [None, None, None]
        }
        for s in ('l', 'r'):
            lowerarm = self.model.pose.bones[f'lowerarm_{s}']
            lowerarm.constraints.new('IK')
            lowerarm.constraints['IK'].target = self.model
            lowerarm.constraints['IK'].subtarget = f'hand_{s}_IK'
            lowerarm.constraints['IK'].chain_count = 2

            hand = self.model.pose.bones[f'hand_{s}']
            hand.constraints.new("COPY_ROTATION")
            hand.constraints['Copy Rotation'].enabled = False
            hand.constraints['Copy Rotation'].target = self.model
            hand.constraints['Copy Rotation'].subtarget = f'hand_{s}_IK'

            for name, consts in ik_constraints.items():
                bone = self.model.pose.bones[f'{name}_{s}']
                for axe, const in zip(('x', 'y', 'z'), consts):
                    if const is None:
                        setattr(bone, f'lock_ik_{axe}', True)
                    else:
                        setattr(bone, f'lock_ik_{axe}', False)
                        setattr(bone, f'use_ik_limit_{axe}', True)
                        setattr(bone, f'ik_min_{axe}', utils.r(const[0]))
                        setattr(bone, f'ik_max_{axe}', utils.r(const[1]))

        self.model.pose.bones[f'hand_{s}_IK'].location = Vector((0, 1.2, 0.25))

        for i in '123':
            self.model.pose.bones[f'spine_0{i}'].lock_ik_x = False
            self.model.pose.bones[f'spine_0{i}'].use_ik_limit_x = True
            self.model.pose.bones[f'spine_0{i}'].ik_min_x = utils.r(20) if i == '3' else 0
            self.model.pose.bones[f'spine_0{i}'].lock_ik_z = True

        bpy.ops.object.mode_set(mode='OBJECT')

    def init_textures(self):
        for o in self.model.children:
            if not o.type == 'MESH':
                continue
            if 'Mesh' not in o.name:
                o.name = o.name.split(':')[-1].lower() + "Mesh"
            if "eye" in o.name or "high" in o.name:
                continue
            try:
                for hair in hairs:
                    if hair.lower().replace(' ', '_') in o.name.lower():
                        o.active_material.blend_method = 'HASHED'
                        break
                else:
                    o.active_material.blend_method = 'OPAQUE'

            except AttributeError:
                continue

    def refresh(self, car=None):
        for cloth, name in ((self.top, 'Top'), (self.bot, 'Bot')):
            if cloth is not None:
                cloth.node_tree.nodes["ColorRamp"].color_ramp.elements[0].color = random_HSV()
                cloth.node_tree.nodes["Mix"].inputs[0].default_value = random.uniform(0.25, 0.75)
            else:
                print(name, self.model.name)

        if "Mix" in self.hairs.node_tree.nodes:
            self.hairs.node_tree.nodes["Mix"].inputs[2].default_value = random_color_hair()
        else:
            self.hairs.node_tree.nodes["Principled BSDF"].inputs[0].default_value = random_color_hair()

        set_bounds(self.model, car)


def get_face(model):
    # Compute approximate coordinates of face markers
    previous_mode = bpy.context.mode
    bpy.ops.object.mode_set(mode='OBJECT')
    matrix_world = model.matrix_world.copy()

    face_3D = {}
    for marker in ['nose', 'eye_l', 'eye_r', 'ear_l', 'ear_r']:
        if 'eye' in marker:
            marker_loc = model.pose.bones[marker].head * 0.65 + model.pose.bones[marker].tail * 0.35
        else:
            marker_loc = model.pose.bones[marker].tail
        face_3D[marker] = matrix_world @ marker_loc

    bpy.ops.object.mode_set(mode=previous_mode)
    return face_3D


def get_vis_face(model, cam):
    face_model = None

    ress = {}
    for obj in model.children:
        if 'bodyMesh' in obj.name:
            face_model = obj
            break
    assert face_model is not None
    mw = face_model.matrix_world
    mwi = mw.inverted()

    ray_begin = mwi @ cam.location
    for name in ['nose', 'eye_l', 'eye_r', 'ear_l', 'ear_r']:
        head = utils.get_head_pose(name, model)
        tail = utils.get_tail_pose(name, model)
        if 'ear' in name:
            point = tail + 0.25 * (tail - head)
        elif 'eye' in name:
            point = head + 0.5 * (tail - head)
        elif 'nose' in name:
            point = tail + 0.05 * (tail - head)

        print(name, point)
        ray_end = mwi @ point
        ray_direction = ray_end - ray_begin
        ray_dist = ray_direction.length
        ray_direction.normalize()
        res = face_model.ray_cast(ray_begin, ray_direction, distance=ray_dist)

        ress[name] = mw @ res[1] if res[0] else None
    return ress

def random_HSV():
    color_hsv = [random.random() for _ in range(3)]
    # color_hsv[1] *= 0.8  # threshold saturation
    rgb_color = colorsys.hsv_to_rgb(*color_hsv)

    return list(rgb_color) + [1, ]


def random_color_hair():
    color_hsv = [random.uniform(0.06, 0.12), 0.7, random.random() ** 3 * 0.8]
    rgb_color = colorsys.hsv_to_rgb(*color_hsv)

    return list(rgb_color) + [1, ]


def switch_constraints(model, enable=False):
    for s in 'lr':
        hand_ik = model.pose.bones[f'hand_{s}_IK']
        for constr in hand_ik.constraints:
            constr.enabled = enable
        model.pose.bones[f'hand_{s}'].constraints['Copy Rotation'].enabled = not enable
        model.pose.bones[f'lowerarm_{s}'].constraints['IK'].chain_count = 2


def set_bounds(model, car=None):
    set_shrinkwraps(model, car)


def set_shrinkwraps(model, car=None):
    original_mode = bpy.context.mode
    utils.select_only(model)
    bpy.ops.object.mode_set(mode='POSE')

    out_objects = None
    if car is not None:
        out_objects = [ch for ch in car.children_recursive if (ch.name[:4] == 'OUT_' or ch.name[:3] == 'IN_')]

    for s in 'lr':
        bone = model.pose.bones[f'hand_{s}_IK']
        [bone.constraints.remove(constr) for constr in bone.constraints if 'Shrinkwrap' in constr.name]
        if car is None:
            continue
        for obj in out_objects:
            constr = bone.constraints.new('SHRINKWRAP')
            constr.target = obj
            constr.wrap_mode = 'OUTSIDE' if obj.name[:4] == 'OUT_' else 'INSIDE'
            if 'Back' in obj.name:
                constr.distance = model.pose.bones['lowerarm_l'].length * model.scale.x / obj.scale.y * 1
            if 'Side' in obj.name:
                constr.distance = model.pose.bones[
                                      'lowerarm_l'].length * model.scale.x / obj.scale.z * 0.2
            else:
                constr.distance = model.pose.bones['hand_l'].length * model.scale.x / obj.scale.z * 1.5

    bpy.ops.object.mode_set(mode=original_mode)


class HumanLoader:
    def __init__(self, dir_path, max_len=10):
        self.human_paths = glob.glob(os.path.join(dir_path, '*.mhx2'))
        random.shuffle(self.human_paths)
        self.paths = {}
        self.humans = {}
        self.max_len = max_len
        self.start_loaded = 0
        self.current_idx = -1

        self.collection = D.collections.new("Humans")
        C.scene.collection.children.link(self.collection)

        self.picker = None
        # self.load_next()

    def load_next(self):
        if self.max_len >= len(self.human_paths):
            # If asked too much
            self.start_loaded = 0
            self.max_len = len(self.human_paths)
            if len(self.paths) == len(self.human_paths):
                # If  everything already loaded
                return
            else:
                end_loaded = len(self.human_paths)
        else:
            end_loaded = self.start_loaded + self.max_len

        human_paths = (self.human_paths * 2)[self.start_loaded:end_loaded]
        human_paths = list({k: None for k in human_paths})

        already_loaded = [hp for hp in human_paths if hp in self.paths.values()]
        self.paths = {k: v for k, v in self.paths.items() if v in already_loaded}
        self.humans = {k: v for k, v in self.humans.items() if k in already_loaded}
        clear_humans(exceptions=list(self.paths))
        for human_path in human_paths:
            if human_path in already_loaded:
                continue
            bpy.ops.import_scene.makehuman_mhx2(filepath=os.path.abspath(human_path))

            model = C.active_object
            self.paths[model] = human_path
            self.move_human(model)
            armature = D.armatures[model.name]
            armature.show_axes = True
            armature.display_type = "OCTAHEDRAL"
            model.show_in_front = False
            self.humans[human_path] = Human(model)

        self.picker = utils.Randomizer(list(self.humans.values()))
        self.start_loaded = end_loaded % len(self.human_paths)

    def next(self, car=None):
        self.current_idx += 1
        if not self.current_idx % self.max_len:
            self.load_next()
            self.current_idx = 0
        choice = self.picker.get(self.current_idx)
        choice.refresh(car=car)
        return choice

    def move_human(self, obj):
        for ch in obj.children:
            self.move_human(ch)

        if obj.name not in self.collection.objects:
            for col in obj.users_collection:
                if col not in [C.collection, self.collection.objects]:
                    D.collections.remove(col)
            self.collection.objects.link(obj)

        utils.select_only(obj)

    def __call__(self, car=None):
        choice = self.picker()
        choice.refresh(car=car)
        return choice


def clear_humans(exceptions=[]):
    collection = D.collections["Humans"]
    exceptions = exceptions + [ch for obj in exceptions for ch in obj.children_recursive]

    for obj in collection.objects:
        if obj in exceptions:
            continue
        for ch in obj.children_recursive + [obj]:
            obj_data = ch.data
            if isinstance(obj_data, bpy.types.Armature):
                D.armatures.remove(obj_data)
            elif isinstance(obj_data, bpy.types.Mesh):
                D.meshes.remove(obj_data)
            try:
                D.objects.remove(obj_data)
            except ReferenceError:
                pass
